# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Components and tools

| **Basic components**                             | **Done** |
| :----------------------------------------------- | :-------:| 
| Basic control tools                              | ✔       | 
| Text input                                       | ✔       |  
| Cursor icon                                      | ✔       |          
| Refresh button                                   | ✔       | 

| **Advanced tools**                               | **Done** |
| :----------------------------------------------- | :-------: |
| Different brush shapes                           | ✔         |
| Un/Re-do button                                  | ✔         | 
| Image tool                                       | ✔         | 
| Download                                         | ✔         | 

| **Other useful widgets**                         | **Check** |
| :----------------------------------------------- | :-------: |
| Line                                             | ✔       |
| Ellipse                                          | ✔       |


---

### How to use 

    按一個工具(預設為鉛筆)，接著在畫布上按下滑鼠後拖曳，就會有與圖案對應的功能出現在畫布上。
    
    如果工具為文字，則是在點擊畫布後立即產生文字方塊。
    
    如果工具為下載、上傳、上一步、下一步和重來等，則在按下按鈕後立即作用。
    
    調色盤只要在上面直接點擊就可，可以滑動、套用在各種有顏色的工具上。
    
    可以選擇線條粗細，可以套用在各種有線條的工具上。
    
    字體的大小跟字形在文字方塊按下Enter後才會套用在畫布上，並不會出現在文字方塊中。
    
    避免誤觸重整頁面跟誤觸重來，可以再這兩種情況後回到上一步。
    
    從文字方塊切換到其他功能時，會把未完成的文字方塊隱形。
    
    上傳圖片會在上次畫布上點擊的位置。

### Function description

    Line：在按下線條按鈕後，在畫布上拖曳出一段線條，可以套用選擇顏色與粗細。
    
    Ellipse：在按下橢圓按鈕(Elip)後，在畫布上拖曳出一段橢圓，可以套用選擇顏色與粗細。

### Gitlab page link

    https://107062104.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    作業比想像中的還難且花時間QQ。

<style>
table th{
    width: 100%;
}
</style>