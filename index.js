var palette = document.getElementById('myPalette');
var palettectx = palette.getContext('2d');
var selectedColor, Size;
var colorTimer;
var color = document.getElementById('myColor');
var colorctx = color.getContext('2d');
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');
var tool = 'pencil';
var isDrawing, isErasing, isRecting, isCircing, isTriaing, isPicking, isLining, isEliping;
var x, x0, x1, x2, y, y0, y1, y2;
var txt = document.querySelector('.textbox');
var fnt = document.getElementById('font');
var fntSize = document.getElementById('fontSize');
var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
window.history.pushState(state, null);
var geo;

// Build Color palette
buildColorPalette = function() {
  var gradient =palettectx.createLinearGradient(0, 0, 256, 0);
  // Create color gradient
  gradient.addColorStop(0,    "rgb(255,   0,   0)");
  gradient.addColorStop(0.15, "rgb(255,   0, 255)");
  gradient.addColorStop(0.33, "rgb(0,     0, 255)");
  gradient.addColorStop(0.49, "rgb(0,   255, 255)");
  gradient.addColorStop(0.67, "rgb(0,   255,   0)");
  gradient.addColorStop(0.84, "rgb(255, 255,   0)");
  gradient.addColorStop(1,    "rgb(255,   0,   0)");
  // Apply gradient to canvas
  palettectx.fillStyle = gradient;
  palettectx.fillRect(0, 0, 256, 256);

  // Create semi transparent gradient (white -> trans. -> black)
  gradient = palettectx.createLinearGradient(0, 0, 0, 256);
  gradient.addColorStop(0,   "rgba(255, 255, 255, 1)");
  gradient.addColorStop(0.5, "rgba(255, 255, 255, 0)");
  gradient.addColorStop(0.5, "rgba(0,     0,   0, 0)");
  gradient.addColorStop(1,   "rgba(0,     0,   0, 1)");
  // Apply gradient to palette
  palettectx.fillStyle = gradient;
  palettectx.fillRect(0, 0, 256, 256);
};

//set color paletter and default color black
buildColorPalette();
colorctx.fillStyle = 'black';
colorctx.fillRect(0, 0, color.width, color.height);

//detect palette color
palette.onmousedown = function(e) {
  isPicking = true;
  imageData = palettectx.getImageData(e.offsetX, e.offsetY, 1, 1);
  selectedColor = 'rgb(' + imageData.data[0] + ', ' + imageData.data[1] + ', ' + imageData.data[2] + ')';
  // Track mouse movement on the canvas if the mouse button is down
  palette.onmousemove = function(e){
    if(isPicking){
      imageData = palettectx.getImageData(e.offsetX, e.offsetY, 1, 1);
      selectedColor = 'rgb(' + imageData.data[0] + ', ' + imageData.data[1] + ', ' + imageData.data[2] + ')';
    }
  }
  colorTimer = setInterval(function(){
    colorctx.fillStyle = selectedColor;
    colorctx.fillRect(0, 0, 41, 41);
  }, 50);
};
palette.onmouseup= function(e) {
  clearInterval(colorTimer);
  isPicking = false;
};

//set pencil
document.getElementById('pencil').addEventListener('click',function(){
  if(geo!=null){
    document.body.removeChild(geo);
    geo = null;
  }
  if(tool == 'text'){
    txt.style.display = 'none';
    txt.value = '';
  }
  tool = 'pencil';
},false);

//set eraser
document.getElementById('eraser').addEventListener('click',function(){
  if(geo!=null){
    document.body.removeChild(geo);
    geo = null;
  }
  if(tool == 'text'){
    txt.style.display = 'none';
    txt.value = '';
  }
  tool = 'eraser';
},false);

//set text
document.getElementById('text').addEventListener('click',function(){
  if(geo!=null){
    document.body.removeChild(geo);
    geo = null;
  }
  if(tool == 'text'){
    txt.style.display = 'none';
    txt.value = '';
  }
  tool = 'text';
},false);

//triangle
document.getElementById('triangle').addEventListener('click',function(){
  if(tool == 'text'){
    txt.style.display = 'none';
    txt.value = '';
  }
  tool = 'triangle';
  if(geo == null){
    geo = document.createElement('canvas');
    geoctx = geo.getContext('2d');
    geo.id = 'geo';
    geo.classList.add('myCanvas');
    geo.width = canvas.width;
    geo.height = canvas.height;
    document.body.appendChild(geo);
    geo.style.backgroundColor= 'transparent';
  }
  geo.onmousedown = function(e){
    isTriaing = true;
    x0 = e.offsetX;
    y0 = e.offsetY;
  }
  geo.onmousemove = function(e){
    geoctx.clearRect(0, 0, geo.width, geo.height);
    geoctx.beginPath();
    if(isTriaing){
      geoctx.lineJoin = geoctx.lineCap = 'round';
      geoctx.lineWidth = document.getElementById('size').value;
      geoctx.strokeStyle = selectedColor;
      geoctx.moveTo(x0,y0);
      geoctx.lineTo((e.offsetX+x0)/2, e.offsetY);
      geoctx.lineTo(e.offsetX, y0);
      geoctx.lineTo(x0, y0);
      geoctx.stroke();
    }
    geo.style.cursor='url(img/triangle-cursor.png), auto'; 
  }
  geo.onmouseup = function(e){
    isTriaing = false;
    geoctx.clearRect(0, 0, geo.width, geo.height);
    ctx.beginPath();
    ctx.lineJoin = ctx.lineCap = 'round';
    ctx.lineWidth = document.getElementById('size').value;
    ctx.strokeStyle = selectedColor;
    ctx.moveTo(x0,y0);
    ctx.lineTo((e.offsetX+x0)/2, e.offsetY);
    ctx.lineTo(e.offsetX, y0);
    ctx.lineTo(x0, y0);
    ctx.stroke();
    var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);
  }
},false);

//circle
document.getElementById('circle').addEventListener('click',function(){
  if(tool == 'text'){
    txt.style.display = 'none';
    txt.value = '';
  }
  tool = 'circle';
  if(geo == null){
    geo = document.createElement('canvas');
    geoctx = geo.getContext('2d');
    geo.id = 'geo';
    geo.classList.add('myCanvas');
    geo.width = canvas.width;
    geo.height = canvas.height;
    document.body.appendChild(geo);
    geo.style.backgroundColor= 'transparent';
  }
  geo.onmousedown = function(e){
    isCircing = true;
    x0 = e.offsetX;
    y0 = e.offsetY;
  }
  geo.onmousemove = function(e){
    geoctx.clearRect(0, 0, geo.width, geo.height);
    geoctx.beginPath();
    if(isCircing){
      geoctx.lineWidth = document.getElementById('size').value;
      geoctx.strokeStyle = selectedColor;
      x = e.offsetX - x0;
      y = e.offsetY - y0;
      radius = Math.sqrt(x*x + y*y)/2;
      geoctx.arc((e.offsetX+x0)/2, (e.offsetY+y0)/2, radius, 0, 2 * Math.PI);
      geoctx.stroke();
    }
    geo.style.cursor='url(img/circle-cursor.png), auto'; 
  }
  geo.onmouseup = function(e){
    isCircing = false;
    geoctx.clearRect(0, 0, geo.width, geo.height);
    ctx.beginPath();
    ctx.lineWidth = document.getElementById('size').value;
    ctx.strokeStyle = selectedColor;
    x = e.offsetX - x0;
    y = e.offsetY - y0;
    radius = Math.sqrt(x*x + y*y)/2;
    ctx.arc((e.offsetX+x0)/2, (e.offsetY+y0)/2, radius, 0, 2 * Math.PI);
    ctx.stroke();
    var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);
  }
},false);

//react
document.getElementById('rect').addEventListener('click',function(){
  if(tool == 'text'){
    txt.style.display = 'none';
    txt.value = '';
  }
  tool = 'rect';
  if(geo == null){
    geo = document.createElement('canvas');
    geoctx = geo.getContext('2d');
    geo.id = 'geo';
    geo.classList.add('myCanvas');
    geo.width = canvas.width;
    geo.height = canvas.height;
    document.body.appendChild(geo);
    geo.style.backgroundColor= 'transparent';
  }
  geo.onmousedown = function(e){
    isRecting = true;
    x0 = e.offsetX;
    y0 = e.offsetY;
  }
  geo.onmousemove = function(e){
    geoctx.clearRect(0, 0, geo.width, geo.height);
    //console.log(e.offsetY, e.offsetY, geo.width, geo.height);
    geoctx.beginPath();
    if(isRecting){
      geoctx.lineWidth = document.getElementById('size').value;
      geoctx.strokeStyle = selectedColor;
      geoctx.rect(x0, y0, e.offsetX-x0, e.offsetY-y0);
      geoctx.stroke();
    }
    geo.style.cursor='url(img/rectangle-cursor.png), auto'; 
  }
  geo.onmouseup = function(e){
    isRecting = false;
    geoctx.clearRect(0, 0, rect.width, rect.height);
    ctx.beginPath();
    ctx.lineWidth = document.getElementById('size').value;
    ctx.strokeStyle = selectedColor;
    ctx.rect(x0, y0, e.offsetX-x0, e.offsetY-y0);
    ctx.stroke();
    var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);
  }
},false);

//line
document.getElementById('line').addEventListener('click',function(){
  if(tool == 'text'){
    txt.style.display = 'none';
    txt.value = '';
  }
  tool = 'line';
  if(geo == null){
    console.log(1);
    geo = document.createElement('canvas');
    geoctx = geo.getContext('2d');
    geo.id = 'geo';
    geo.classList.add('myCanvas');
    geo.width = canvas.width;
    geo.height = canvas.height;
    document.body.appendChild(geo);
    geo.style.backgroundColor= 'transparent';
  }
  geo.onmousedown = function(e){
    isLining = true;
    x0 = e.offsetX;
    y0 = e.offsetY;
  }
  geo.onmousemove = function(e){
    geoctx.clearRect(0, 0, geo.width, geo.height);
    geoctx.beginPath();
    if(isLining){
      geoctx.lineJoin = geoctx.lineCap = 'round';
      geoctx.lineWidth = document.getElementById('size').value;
      geoctx.strokeStyle = selectedColor;
      geoctx.moveTo(x0,y0);
      geoctx.lineTo(e.offsetX, e.offsetY);
      geoctx.stroke();
    }
    geo.style.cursor='url(img/line-cursor.png), auto'; 
  }
  geo.onmouseup = function(e){
    isLining = false;
    geoctx.clearRect(0, 0, geo.width, geo.height);
    ctx.beginPath();
    ctx.lineJoin = ctx.lineCap = 'round';
    ctx.lineWidth = document.getElementById('size').value;
    ctx.strokeStyle = selectedColor;
    ctx.moveTo(x0,y0);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.stroke();
    var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);
  }
},false);

//ellipse
document.getElementById('Elip').addEventListener('click',function(){
  if(tool == 'text'){
    txt.style.display = 'none';
    txt.value = '';
  }
  tool = 'Elip';
  if(geo == null){
    console.log(1);
    geo = document.createElement('canvas');
    geoctx = geo.getContext('2d');
    geo.id = 'geo';
    geo.classList.add('myCanvas');
    geo.width = canvas.width;
    geo.height = canvas.height;
    document.body.appendChild(geo);
    geo.style.backgroundColor= 'transparent';
  }
  geo.onmousedown = function(e){
    isEliping = true;
    x0 = e.offsetX;
    y0 = e.offsetY;
  }
  geo.onmousemove = function(e){
    geoctx.clearRect(0, 0, geo.width, geo.height);
    geoctx.beginPath();
    if(isEliping){
      geoctx.lineJoin = geoctx.lineCap = 'round';
      geoctx.lineWidth = document.getElementById('size').value;
      geoctx.strokeStyle = selectedColor;
      geoctx.ellipse(x0, y0, Math.abs(e.offsetX-x0), Math.abs(e.offsetY-y0), Math.PI, 0, 2 * Math.PI);
      geoctx.stroke();
    }
    geo.style.cursor='url(img/elip-cursor.png), auto'; 
  }
  geo.onmouseup = function(e){
    isEliping = false;
    geoctx.clearRect(0, 0, geo.width, geo.height);
    ctx.beginPath();
    ctx.lineJoin = ctx.lineCap = 'round';
    ctx.lineWidth = document.getElementById('size').value;
    ctx.strokeStyle = selectedColor;
    ctx.ellipse(x0, y0, Math.abs(e.offsetX-x0), Math.abs(e.offsetY-y0), Math.PI, 0, 2 * Math.PI);
    ctx.stroke();
    var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);
  }
},false);

//download
document.getElementById('download').addEventListener('click',function(){
  if(tool == 'text'){
    txt.style.display = 'none';
    txt.value = '';
  }
  var download = document.getElementById('download');
  var image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
  download.setAttribute("href", image);
  //download.setAttribute("download","archive.png");
},false);

//upload
document.getElementById('upload').addEventListener('click',function(e){
  if(tool == 'text'){
    txt.style.display = 'none';
    txt.value = '';
  }
  document.getElementById('file').click();
  file.addEventListener('change', function (e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            ctx.drawImage(img,x0,y0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
  }, false);
},false);

//undo
document.getElementById('undo').addEventListener('click',function(){
  if(tool == 'text'){
    txt.style.display = 'none';
    txt.value = '';
  }
  window.history.go(-1);
},false);

//redo
document.getElementById('redo').addEventListener('click',function(){
  if(tool == 'text'){
    txt.style.display = 'none';
    txt.value = '';
  }
  window.history.go(1);
},false);

//reset
document.getElementById('reset').addEventListener('click',function(){
  if(tool == 'text'){
    txt.style.display = 'none';
    txt.value = '';
  }
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
  window.history.pushState(state, null);
},false);

//pencil eraser text detect on canvas
canvas.onmousedown = function(e) {
  x0 = e.offsetX;
  y0 = e.offsetY;
  ctx.lineWidth = document.getElementById('size').value;
  ctx.beginPath();
  ctx.globalCompositeOperation = 'source-over';
  if(tool == 'pencil'){
    isDrawing = true;
  }
  else if(tool == 'eraser'){
    isErasing = true;
  }
  else if(tool == 'text'){
    txt.style.display = 'block';
    txt.style.top = e.pageY + 'px';
    txt.style.left= e.pageX + 'px';
    txt.font = fntSize.options[fntSize.selectedIndex].value + fnt.options[fnt.selectedIndex].value;
    txt.onkeydown = function(e){
      if(e.keyCode === 13){
        ctx.textBaseline = 'top';
        ctx.textAlign = 'left';
        ctx.font = fntSize.options[fntSize.selectedIndex].value + fnt.options[fnt.selectedIndex].value;
        ctx.fillText(txt.value,x0, y0);
        console.log( fnt.options[fnt.selectedIndex].value, fntSize.options[fntSize.selectedIndex].value);
        txt.style.display = 'none';
        txt.value = '';
        var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
        window.history.pushState(state, null);
      }
    }
  }
};

canvas.onmousemove = function(e) {
  ctx.beginPath();  
  if(tool == 'pencil'){
    canvas.style.cursor='url(img/pencil-cursor.png), auto';  
    if (isDrawing) {
      ctx.lineJoin = ctx.lineCap = 'round';
      ctx.moveTo(x1, y1);
      ctx.lineTo(e.offsetX, e.offsetY);
      ctx.strokeStyle = selectedColor;
      ctx.stroke();
    }
  }
  else if(tool == 'eraser'){
    canvas.style.cursor='url(img/eraser-cursor.png), auto';  
    if (isErasing) {
      ctx.globalCompositeOperation = 'destination-out';
      ctx.lineJoin = ctx.lineCap = 'round';
      ctx.moveTo(x1, y1);
      ctx.lineTo(e.offsetX, e.offsetY);
      ctx.stroke();
      ctx.globalCompositeOperation = 'source-over';
    }
  }
  else if(tool == 'text'){
    canvas.style.cursor= 'text';  
  }
  else if(tool == 'triangle'){
  }
  x1 = e.offsetX;
  y1 = e.offsetY;
};

canvas.onmouseup = function(e) { 
  if(tool == 'pencil'){
    isDrawing = false;
  }
  else if(tool == 'eraser'){
    isErasing = false;
  }
  else if(tool == 'text'){
  }
  if(tool!='text'){
    var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);
  }
};

window.addEventListener('popstate', changeStep, false);
function changeStep(e){
  // 清除畫布
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  if(e.state){
    ctx.putImageData(e.state, 0, 0);
  }
}
